import { Observable } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import { inspect } from 'util'
import WebSocket from 'ws'

const MASTODON_SERVER = 'mastodon.social'
const MASTODON_STREAMING_URL = `wss://${MASTODON_SERVER}/api/v1/streaming?stream=public`
const NORMAL_CLOSURE_CODE = 1000

interface MastodonStreamingMessage {
  event: string
  payload: string
}

function webSocketToObservable<T = string>(webSocket: WebSocket): Observable<T> {
  return new Observable<T>(subscriber => {
    const onMessage = (data: T) => subscriber.next(data)
    const onError = (e: Error) => subscriber.error(e)
    const onClose = () => subscriber.complete()

    webSocket.on('message', onMessage)
    webSocket.on('error', onError)
    webSocket.on('close', onClose)

    return () => {
      webSocket.off('message', onMessage)
      webSocket.off('error', onError)
      webSocket.off('close', onClose)
    }
  })
}

async function main() {
  const ws = new WebSocket(MASTODON_STREAMING_URL)

  process.on('SIGINT', () => {
    ws.close(NORMAL_CLOSURE_CODE)
  })

  try {
    await webSocketToObservable(ws)
      .pipe(
        map(data => JSON.parse(data)),
        map(({ event, payload }: MastodonStreamingMessage) => ({ event, payload: JSON.parse(payload) })),
        tap((parsedPayload: object) => console.debug(inspect(parsedPayload, { depth: null, colors: true })))
      )
      .toPromise()
    console.debug('server shutting down')
  } catch (error) {
    console.error(error)
  }
}

main().catch(console.error)
